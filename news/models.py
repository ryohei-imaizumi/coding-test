import os
from django.db import models
from datetime import datetime
from pytz import timezone, utc
from django.db import models
from newsapi import NewsApiClient
import iso8601


class ArticleManager(models.Manager):
    newsapi = NewsApiClient(api_key=os.environ.get('NEWS_API_KEY'))
    def update(self):
        """Run bulk insert at the first time only"""
        if self.first():
            self._insert_articles()
        else:
            self._bulk_insert()

    def _bulk_insert(self):
        headlines = self.newsapi.get_top_headlines(language='en', page_size=100)
        self.bulk_create([self.model(
            title=article['title'] or '',
            description = article['description'] or '',
            url = article['url'] or '',
            url_to_image = article['urlToImage'] or '',
            published_at = iso8601.parse_date(article['publishedAt']),
        ) for article in headlines['articles']])

    def _insert_articles(self):
        """Insert articles that are published later than latest article in the DB"""
        headlines = self.newsapi.get_top_headlines(language='en', page_size=100)
        latest = self.latest('published_at')
        latest_published = latest.published_at

        for article in headlines['articles']:
            published_at = iso8601.parse_date(article['publishedAt'])
            if published_at > latest_published:
                self.insert_article(article)
    
    def insert_article(self, article):
        self.create(
            title=article['title'] or 'No title',
            description = article['description'] or '',
            url = article['url'] or '',
            url_to_image = article['urlToImage'] or '',
            published_at = iso8601.parse_date(article['publishedAt']),
        )

    def latest_articles(self, number):
        return self.order_by('published_at').reverse()[:number]


class Article(models.Model):
    title = models.TextField(blank=True)
    description = models.TextField(blank=True)
    url = models.URLField(blank=True)
    url_to_image = models.URLField(blank=True)
    published_at = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = ArticleManager()

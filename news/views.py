from django.shortcuts import render
from django.views.generic import ListView
from rest_framework import viewsets
from .models import Article
from .serializer import ArticleSerializer


class ArticlesListView(ListView):
    template_name = 'articles_list.html'
    model = Article

    def get_queryset(self):
        Article.objects.update()
        return Article.objects.latest_articles(number=20)


class ArticleViewSet(viewsets.ModelViewSet):
    queryset = Article.objects.latest_articles(number=100)
    serializer_class = ArticleSerializer

    def list(self, request):
        Article.objects.update()
        return super().list(request)

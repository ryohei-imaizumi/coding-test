# Coding-Test setup flow

Python 3.6.5

Django 2.1.7

## Git clone
Clone this project
```
git clone https://ryohei-imaizumi@bitbucket.org/ryohei-imaizumi/coding-test.git
```

then move to the folder
```
cd coding-test/
```

## Install dependencies
Install the dependencies by following the command after changing your environment(replace $yourenv$ in the command below).
```
pyenv local $yourenv$
pip install --upgrade pip
pip install -r requirements.txt
```

## Migrate database
Once you make sure you are in the folder that contains manage.py, run the command below.
```
python manage.py migrate
```

## Run django web server and browse
You are also required to be in the folder containing manage.py.
Replace "\$API_KEY$" with your API Key, and then run this command.

Mac or Linux
```
export NEWS_API_KEY="$API_KEY$"
python manage.py runserver
```

Windows
```
set NEWS_API_KEY=$API_KEY$
python manage.py runserver
```

Listing 20 latest articles page is 
http://127.0.0.1:8000/list/

API endpoint for 100 latest articles is
http://127.0.0.1:8000/api/articles/

Or you also can use curl command to get JSON format data
```
curl http://127.0.0.1:8000/api/articles/
```
